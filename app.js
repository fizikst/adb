var Selector = (function(){

    var host = "http://nqhudqqqhg.ru:8001";
    var interval = 1000;
    var inc = 0;

    return {
        storage : {},
        selectors: {},
        init: function(){
            this.getSelectors();
        },
        getSelectors: function(){
            this.ajax(host + '/blocks', function (data) {
                var list = JSON.parse(data.responseText);
                if (Object.keys(list).length > 0 ) {
                    this.selectors = list;
                    this.find();
                } else {
                    return;
                }
            }.bind(this));
        },
        find: function () {
                var list = this.selectors;
                for(var k in list) {
                    try {
                        var nodes = document.evaluate(list[k].xpath, document, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
                        var node = nodes.iterateNext();

                        while (node) {
                            var id = this.getId(node);
                            this.storage[id] = node;
                            node = nodes.iterateNext();
                        }
                    }
                    catch (e) {
                        console.log( 'Error: Document tree modified during iteration ' + e );
                    }
                }
                this.detect();
        },
        detect: function () {
            setInterval(
                (function(self) {
                    return function () {
                        for (var k in self.storage) {
                            var node = self.storage[k];
                            if (node.offsetWidth >= 30 && node.offsetHeight >= 30 && (node.active===undefined || node.active === 0)) {
                                node.active = 1;
                                self.getBanner(node);
                            }
                        }
                    }
                })(this)
                ,interval);
        },
        getBanner : function(node) {
            var width = node.offsetWidth;
            var height = node.offsetHeight;

            this.ajax(host+'/banners?height=' + height + '&width=' + width, function (data) {
                var banner = JSON.parse(data.responseText)[0];
                this.render(node, banner);
            }.bind(this));
        },
        render: function(node, banner){
            node.innerHTML = banner.html;
        },
        ajax: function (url, cb) {
            var xhr;
            if(typeof XMLHttpRequest !== 'undefined') {
                xhr = new XMLHttpRequest();
            }

            xhr.onreadystatechange = ensureReadiness;

            function ensureReadiness() {
                if(xhr.readyState < 4 || xhr.status !== 200) {
                    return;
                }
                if(xhr.readyState === 4 && xhr.status === 200) {
                    cb(xhr);
                }
            }

            xhr.open('GET', url, true);
            xhr.send('');
        },
        getId : function(node) {
            if (!node.id) {
                node.id = "id_" + inc++;
            }
            return node.id;
        }
}
})();

/**
 * @module Application
 */
var App = (function(){
    return {
        init: function(){
            Selector.init();
        }
    }
})();
App.init();

//*[@id="right-column"]/div[1]
